## Values вынесены в отдельный private репозиторий

```
---
# Source: flask-project/templates/namespace.yml
apiVersion: v1
kind: Namespace
metadata:
  name: flask-project
---
# Source: flask-project/templates/dashboard.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: flask-project-dashboards
  namespace: monitoring
  labels:
    grafana_dashboard: "1"
data:
  flask_project_dashboard.json: |
    {
      "annotations": {
        "list": [
          {
            "builtIn": 1,
            "datasource": {
              "type": "grafana",
              "uid": "-- Grafana --"
            },
            "enable": true,
            "hide": true,
            "iconColor": "rgba(0, 211, 255, 1)",
            "name": "Annotations & Alerts",
            "type": "dashboard"
          }
        ]
      },
      "editable": true,
      "fiscalYearStartMonth": 0,
      "graphTooltip": 0,
      "id": 33,
      "links": [],
      "panels": [
        {
          "datasource": {
            "type": "prometheus",
            "uid": "prometheus"
          },
          "fieldConfig": {
            "defaults": {
              "color": {
                "mode": "palette-classic"
              },
              "custom": {
                "axisBorderShow": false,
                "axisCenteredZero": false,
                "axisColorMode": "text",
                "axisLabel": "",
                "axisPlacement": "auto",
                "barAlignment": 0,
                "drawStyle": "line",
                "fillOpacity": 25,
                "gradientMode": "none",
                "hideFrom": {
                  "legend": false,
                  "tooltip": false,
                  "viz": false
                },
                "insertNulls": false,
                "lineInterpolation": "linear",
                "lineWidth": 1,
                "pointSize": 5,
                "scaleDistribution": {
                  "type": "linear"
                },
                "showPoints": "auto",
                "spanNulls": false,
                "stacking": {
                  "group": "A",
                  "mode": "none"
                },
                "thresholdsStyle": {
                  "mode": "off"
                }
              },
              "mappings": [],
              "thresholds": {
                "mode": "absolute",
                "steps": [
                  {
                    "color": "green",
                    "value": null
                  },
                  {
                    "color": "red",
                    "value": 80
                  }
                ]
              }
            },
            "overrides": []
          },
          "gridPos": {
            "h": 8,
            "w": 24,
            "x": 0,
            "y": 0
          },
          "id": 1,
          "options": {
            "legend": {
              "calcs": [],
              "displayMode": "hidden",
              "placement": "right",
              "showLegend": false
            },
            "tooltip": {
              "maxHeight": 600,
              "mode": "single",
              "sort": "none"
            }
          },
          "targets": [
            {
              "datasource": {
                "type": "prometheus",
                "uid": "prometheus"
              },
              "expr": "avg(sum by (pod) (rate(container_cpu_usage_seconds_total{pod=~\"flask-project-deployment-.*\"}[5m])))",
              "format": "time_series",
              "interval": "",
              "legendFormat": "CPU Usage",
              "refId": "A"
            }
          ],
          "title": "Average CPU Usage",
          "type": "timeseries"
        },
        {
          "datasource": {
            "type": "prometheus",
            "uid": "prometheus"
          },
          "fieldConfig": {
            "defaults": {
              "color": {
                "mode": "palette-classic"
              },
              "custom": {
                "axisBorderShow": false,
                "axisCenteredZero": false,
                "axisColorMode": "text",
                "axisLabel": "",
                "axisPlacement": "auto",
                "barAlignment": 0,
                "drawStyle": "line",
                "fillOpacity": 25,
                "gradientMode": "none",
                "hideFrom": {
                  "legend": false,
                  "tooltip": false,
                  "viz": false
                },
                "insertNulls": false,
                "lineInterpolation": "linear",
                "lineWidth": 1,
                "pointSize": 5,
                "scaleDistribution": {
                  "type": "linear"
                },
                "showPoints": "auto",
                "spanNulls": false,
                "stacking": {
                  "group": "A",
                  "mode": "none"
                },
                "thresholdsStyle": {
                  "mode": "off"
                }
              },
              "mappings": [],
              "thresholds": {
                "mode": "absolute",
                "steps": [
                  {
                    "color": "green",
                    "value": null
                  },
                  {
                    "color": "red",
                    "value": 80
                  }
                ]
              }
            },
            "overrides": []
          },
          "gridPos": {
            "h": 8,
            "w": 24,
            "x": 0,
            "y": 8
          },
          "id": 2,
          "options": {
            "legend": {
              "calcs": [],
              "displayMode": "hidden",
              "placement": "right",
              "showLegend": false
            },
            "tooltip": {
              "maxHeight": 600,
              "mode": "single",
              "sort": "none"
            }
          },
          "targets": [
            {
              "datasource": {
                "type": "prometheus",
                "uid": "prometheus"
              },
              "expr": "avg(sum by (pod) (container_memory_usage_bytes{pod=~\"flask-project-deployment-.*\"}) / (1024 * 1024))",
              "format": "time_series",
              "interval": "",
              "legendFormat": "Memory Usage (Mi)",
              "refId": "A"
            }
          ],
          "title": "Average Memory Usage",
          "type": "timeseries"
        },
        {
          "datasource": {
            "type": "prometheus",
            "uid": "prometheus"
          },
          "fieldConfig": {
            "defaults": {
              "mappings": [],
              "max": 100,
              "min": 0,
              "thresholds": {
                "mode": "absolute",
                "steps": [
                  {
                    "color": "green",
                    "value": null
                  },
                  {
                    "color": "red",
                    "value": 80
                  }
                ]
              }
            },
            "overrides": []
          },
          "gridPos": {
            "h": 8,
            "w": 12,
            "x": 0,
            "y": 16
          },
          "id": 3,
          "options": {
            "minVizHeight": 75,
            "minVizWidth": 75,
            "orientation": "auto",
            "reduceOptions": {
              "calcs": [
                "lastNotNull"
              ],
              "fields": "",
              "values": false
            },
            "showThresholdLabels": false,
            "showThresholdMarkers": true,
            "sizing": "auto"
          },
          "pluginVersion": "11.0.0",
          "targets": [
            {
              "datasource": {
                "type": "prometheus",
                "uid": "prometheus"
              },
              "expr": "sum by (pod) (kube_pod_container_resource_limits{pod=~\"flask-project-deployment-.*\", resource=\"memory\"}) / (1024 * 1024)",
              "format": "time_series",
              "interval": "",
              "legendFormat": "Memory Limits (Mi)",
              "refId": "A"
            }
          ],
          "title": "Memory Limits",
          "type": "gauge"
        },
        {
          "datasource": {
            "type": "prometheus",
            "uid": "prometheus"
          },
          "fieldConfig": {
            "defaults": {
              "mappings": [],
              "max": 100,
              "min": 0,
              "thresholds": {
                "mode": "absolute",
                "steps": [
                  {
                    "color": "green",
                    "value": null
                  },
                  {
                    "color": "red",
                    "value": 80
                  }
                ]
              }
            },
            "overrides": []
          },
          "gridPos": {
            "h": 8,
            "w": 12,
            "x": 12,
            "y": 16
          },
          "id": 4,
          "options": {
            "minVizHeight": 75,
            "minVizWidth": 75,
            "orientation": "auto",
            "reduceOptions": {
              "calcs": [
                "lastNotNull"
              ],
              "fields": "",
              "values": false
            },
            "showThresholdLabels": false,
            "showThresholdMarkers": true,
            "sizing": "auto"
          },
          "pluginVersion": "11.0.0",
          "targets": [
            {
              "datasource": {
                "type": "prometheus",
                "uid": "prometheus"
              },
              "expr": "sum by (pod) (kube_pod_container_resource_limits{pod=~\"flask-project-deployment-.*\", resource=\"cpu\"})",
              "format": "time_series",
              "interval": "",
              "legendFormat": "CPU Limits",
              "refId": "A"
            }
          ],
          "title": "CPU Limits",
          "type": "gauge"
        },
        {
          "datasource": {
            "type": "prometheus",
            "uid": "prometheus"
          },
          "fieldConfig": {
            "defaults": {
              "mappings": [],
              "thresholds": {
                "mode": "absolute",
                "steps": [
                  {
                    "color": "green",
                    "value": null
                  },
                  {
                    "color": "red",
                    "value": 80
                  }
                ]
              },
              "unit": "none"
            },
            "overrides": []
          },
          "gridPos": {
            "h": 8,
            "w": 24,
            "x": 0,
            "y": 24
          },
          "id": 5,
          "options": {
            "colorMode": "none",
            "graphMode": "none",
            "justifyMode": "auto",
            "orientation": "horizontal",
            "reduceOptions": {
              "calcs": [
                "lastNotNull"
              ],
              "fields": "",
              "values": false
            },
            "showPercentChange": false,
            "textMode": "auto",
            "wideLayout": true
          },
          "pluginVersion": "11.0.0",
          "targets": [
            {
              "datasource": {
                "type": "prometheus",
                "uid": "prometheus"
              },
              "expr": "count(kube_pod_info{pod=~\"flask-project-deployment-.*\"})",
              "format": "time_series",
              "interval": "",
              "refId": "A"
            }
          ],
          "title": "Current Number of Pods",
          "type": "stat"
        }
      ],
      "schemaVersion": 39,
      "tags": [],
      "templating": {
        "list": []
      },
      "time": {
        "from": "now-5m",
        "to": "now"
      },
      "timeRangeUpdatedDuringEditOrView": false,
      "timepicker": {},
      "timezone": "",
      "title": "Flask Project Deployment Metrics",
      "uid": "cdpz2ajzf4ohse",
      "version": 2,
      "weekStart": ""
    }
---
# Source: flask-project/templates/service.yml
apiVersion: v1
kind: Service
metadata:
  name: flask-project-service
  namespace: flask-project
  labels:
    app: flask-project
spec:
  type: ClusterIP
  ports:
    - port: 80
      protocol: TCP
      targetPort: 8000
  selector:
    app: flask-project
---
# Source: flask-project/templates/deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: flask-project-deployment
  namespace: flask-project
  labels:
    app: flask-project
spec:
  revisionHistoryLimit: 10
  replicas: 1
  selector:
    matchLabels:
      app: flask-project
  template:
    metadata:
      labels:
        app: flask-project
    spec:
      containers:
      - name: flask-project
        image: registry.gitlab.com/va1erify-deusops/task-18-05-24/development/flask-project/app:v1.0.0
        imagePullPolicy: Always
        ports:
        - name: flask-project
          containerPort: 80
        resources:
          requests:
            memory: 64Mi
            cpu: 10m
          limits:
            memory: 128Mi
            cpu: 40m
---
# Source: flask-project/templates/hpa.yml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: flask-project-hpa
  namespace: flask-project
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: flask-project-deployment
  minReplicas: 1
  maxReplicas: 3
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 80
---
# Source: flask-project/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: flask-project-ingress
  namespace: flask-project
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: letsencrypt-prod
    nginx.ingress.kubernetes.io/proxy-read-timeout: "6000"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "6000"
spec:
  tls:
  - hosts:
    - flask-project.dev.va1erify.ru
    secretName: flask-project-tls
  rules:
  - host: flask-project.dev.va1erify.ru
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: flask-project-service
            port:
              number: 80

```